#Fedora Post Installer#
Please visit the wiki for more information:[Here](https://bitbucket.org/techdragongames/post-linux-installer/wiki/browse/)
* * *
To use the script, paste the commands below into a terminal session.

The point of this script is to install essential third party repositories and software to make the distribution you are using easier to use. These things include; Third party codecs, Media players, propriatery repositories and non-free software.

Feel free to download, edit and share this script with others.
* * *

### Put These Commands into a terminal ###
                  
**Fedora 29**
```
#!bash

wget https://bitbucket.org/techdragongames/post-linux-installer/raw/master/Fedora-Install-Scripts/install.sh

sudo bash install.sh
```