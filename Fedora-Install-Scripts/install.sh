#!/bin/bash
#Title: install.sh
#Description: This will add the rpmfusion repositories and install handy applications.
#Author: Callum-James Smith (TechandDragons)
#Date: 12/12/2018
#Version: 3.0
#Usage: sudo bash install.sh
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


PS3='Please enter your choice: '
options=("Continue" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Continue")
        echo "#_________________________________________________________________#"
	echo "#                  Warning! This may Take a while.                #"
	echo "#_________________________________________________________________#"
	
	sudo dnf install gnome-tweak-tool -y
	
	echo "#                          Installing Fedy                        #"
	echo "#_________________________________________________________________#"
	
	sudo dnf install https://dl.folkswithhats.org/fedora/$(rpm -E %fedora)/RPMS/fedy-release.rpm
	sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
	sudo dnf install fedy
	
	echo "#                          Installing Tools                       #"
	echo "#_________________________________________________________________#"
	
	sudo dnf install icedtea-web java-openjdk -y
	sudo dnf install gstreamer1-plugins-base gstreamer1-plugins-good gstreamer1-plugins-ugly gstreamer1-plugins-bad-free gstreamer1-plugins-bad-free gstreamer1-plugins-bad-freeworld gstreamer1-plugins-bad-free-extras ffmpeg -y
	sudo dnf install vlc -y
	sudo dnf install wine -y
	sudo dnf update -y

	echo "#                          Installing Extras                      #"
	echo "#_________________________________________________________________#"
	
	echo "__Installing Visual Studio Code__"
	sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc -y
	sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
	sudo dnf check-update -y
	sudo dnf install code -y
	echo "__Installing Git__"
	sudo dnf install git -y
	echo "__Installing GitKraken__"
	sudo dnf install libXScrnSaver -y
	sudo dnf check-update && dnf upgrade -y
	wget https://release.gitkraken.com/linux/gitkraken-amd64.tar.gz
	tar -xvzf gitkraken-amd64.tar.gz
	sudo rsync -va --delete-after gitkraken/ /opt/GitKraken/
	cd /opt/GitKraken
	sudo ./gitkraken
	cd
	
	echo "#_________________________________________________________________#"
	echo "#    All Done :); It's reccomended to reboot your system.         #"
        echo "#_________________________________________________________________#"
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
done
